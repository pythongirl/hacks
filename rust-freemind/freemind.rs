type NodeId = u32;

enum Position {
    Left,
    Right
}

enum LinkTarget {
    Url(String),
    Node(NodeId)
}

struct Map {
    version: String,
    root: Node
}

struct Node {
    id: NodeId,
    text: String,
    link: Option<String>,
    folded: Option<bool>,
    color: Option<String>, // Find or make color datatype
    position: Option<Position>,
    
    children: Vec<Node>,
    extra_styles: Vec<ExtraStyle>
}


// TODO: implement a node builder or new function

enum ExtraStyle {
    Edge {
        style: Option<String>,
        color: Option<String>,
        width: Option<i32>
    },
    Font {
        name: String,
        size: i32,
        bold: bool,
        italic: bool
    },
    Icon {
        builtin: String
    },
    Cloud {
        color: String,
    },
    ArrowLink {
        color: String, 
        destination: NodeId,
        startarrow: Option<String>,
        endarrow: Option<String>,
    }
}


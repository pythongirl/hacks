use rand;
use std::fmt;

#[derive(PartialEq, Eq, Clone)]
struct UUID(u128);

fn hex_digit_to_value(b: u8) -> Option<u8> {
    Some(match b {
        b'0' => 0,
        b'1' => 1,
        b'2' => 2,
        b'3' => 3,
        b'4' => 4,
        b'5' => 5,
        b'6' => 6,
        b'7' => 7,
        b'8' => 8,
        b'9' => 9,
        b'a' | b'A' => 0xa,
        b'b' | b'B' => 0xb,
        b'c' | b'C' => 0xc,
        b'd' | b'D' => 0xd,
        b'e' | b'E' => 0xe,
        b'f' | b'F' => 0xf,
        _ => return None,
    })
}

#[derive(Debug, Clone, PartialEq, Eq)]
enum ParseUUIDError {
    InvalidLength,
    ExpectedDash,
    InvalidDigit,
}

impl UUID {
    pub fn new_v4() -> UUID {
        UUID::from_parts(rand::random(), 4, 1)
    }

    pub fn from_bytes(bytes: impl AsRef<[u8]>) -> Result<UUID, ParseUUIDError> {
        let data = bytes.as_ref();
        if data.len() != 36 {
            return Err(ParseUUIDError::InvalidLength);
        }
        let data = &data[0..36]; // This eliminates all bounds checks.
        if data[8] != b'-' || data[13] != b'-' || data[18] != b'-' || data[23] != b'-' {
            return Err(ParseUUIDError::ExpectedDash);
        }
        let digits = data[0..8]
            .iter()
            .chain(data[9..13].iter())
            .chain(data[14..18].iter())
            .chain(data[19..23].iter())
            .chain(data[24..36].iter())
            .map(|&d| hex_digit_to_value(d))
            .collect::<Option<Vec<u8>>>()
            .ok_or(ParseUUIDError::InvalidDigit)?;
        Ok(UUID(
            digits.iter().enumerate().fold(0u128, |acc, (i, &digit)| {
                acc | ((digit as u128) << ((31 - i) * 4))
            }),
        ))
    }

    const fn from_parts(mut base: u128, version: u8, variant: u8) -> UUID {
        const VERSION_BITMASK: u128 = !(0b1111 << (19 * 4));
        base &= VERSION_BITMASK;
        base |= (version as u128) << (19 * 4);
        let variant_bits = ((1u128 << variant) - 1) << (16 * 4 - variant);
        let variant_padding_bitmask = !(1u128 << (16 * 4 - (variant + 1)));
        base |= variant_bits;
        base &= variant_padding_bitmask;
        UUID(base)
    }

    const NIL: UUID = UUID::from_parts(0, 0, 0);
}

impl fmt::Display for UUID {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let write_bytes = |f: &mut fmt::Formatter, bytes: &[u8]| bytes.iter().try_fold((), |_, b| write!(f, "{:02x}", b));
        let bytes = self.0.to_be_bytes();
        write_bytes(f, &bytes[0..4])?;
        write!(f, "-")?;
        write_bytes(f, &bytes[4..6])?;
        write!(f, "-")?;
        write_bytes(f, &bytes[6..8])?;
        write!(f, "-")?;
        write_bytes(f, &bytes[8..10])?;
        write!(f, "-")?;
        write_bytes(f, &bytes[10..16])?;
        Ok(())
    }
}
impl fmt::Debug for UUID {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "UUID({})", self)
    }
}
impl std::str::FromStr for UUID {
    type Err = ParseUUIDError;
    fn from_str(s: &str) -> Result<UUID, ParseUUIDError> {
        UUID::from_bytes(s)
    }
}

fn main() {
    dbg!("123e4567-e89b-12d3-a456-426655440000"
        .parse::<UUID>()
        .unwrap());
    dbg!(UUID::new_v4());
    dbg!(UUID::NIL);
}

use std::thread;

fn main() {

    let child = thread::spawn(|| {
        4 + 2
    });

    let t = 5 + 3;

    let result = child.join().unwrap();

    println!("{}", t * result);
}

use std::fmt;

#[derive(Debug, Copy, Clone)]
enum Color {
    Red,
    Blue,
}

impl fmt::Display for Color {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", match self {
            Color::Red => "R",
            Color::Blue => "B",
        })
    }
}

#[derive(Default, Debug, Clone)]
struct GameBoard {
    board: [[Option<Color>; 6]; 7]
}

impl fmt::Display for GameBoard {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for row in (0..6).rev() {
            for slot in 0..7 {
                match &self.board[slot][row] {
                    Some(token) => {
                        write!(f, "{} ", token)?;
                    }
                    None => {
                        write!(f, "O ")?;
                    }
                }
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

impl GameBoard {
    fn new() -> GameBoard {
        Default::default()
    }
    
    fn from_sequence(i: impl IntoIterator<Item = usize>) -> Result<GameBoard, &'static str> {
        let iter = i.into_iter();
        let mut board = GameBoard::new();
        for (slot, color) in iter.zip((&[Color::Red, Color::Blue]).iter().cloned().cycle()) {
            board.place_token(slot, color)?;
        }
        Ok(board)
    }

    fn place_token(&mut self, slot: usize, color: Color) -> Result<(), &'static str> {
        let stack = self.board.get_mut(slot).ok_or("bad slot")?;
        let open_pos = stack.iter().position(|o| o.is_none()).ok_or("stack full")?;
        stack[open_pos] = Some(color);
        Ok(())
    }
    
    fn available_moves(&self) -> [bool; 7] {
        let mut result = [true; 7];
        for i in 0..7 {
            result[i] = self.board[i][5].is_none();
        }
        result
    }
    
    fn check_winner(&self) -> Option<Color> {
        for x in 0..7 {
            for y in 0..6 {
                for dx in &[-1, 0, 1] {
                    for dy in &[-1, 0, 1] {
                        if 0 
                    }
                }
            }
        }
        
        unimplemented!()
    }
}

fn predict_moves(board: GameBoard, depth: usize) -> Vec<usize> {
    let possible_moves = board.available_moves().iter().enumerate().filter_map(|(i, &available)| if available { Some(i) } else { None }).collect::<Vec<usize>>();
    
    println!("{:?}", possible_moves);
    
    vec![]
}

fn main() -> Result<(), &'static str> {
    let board = GameBoard::from_sequence(vec![0, 1, 0, 2, 0, 3, 0, 4, 0, 5, 0])?;
    
    println!("{}", board);
    
    println!("{:?}", board.available_moves());
    
    predict_moves(board, 0);

    Ok(())
}
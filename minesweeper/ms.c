#include <stdio.h>
#include <ncurses.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

// BUILD: clang -o ms ms.c -Wall -lncurses

void bell(){
	beep();
}

void error(char *message){
	fprintf(stderr, "%s", message);
	exit(1);
}

const int tiles[] = {
	' ' | COLOR_PAIR(13),
	'1' | COLOR_PAIR(1),
	'2' | COLOR_PAIR(2),
	'3' | COLOR_PAIR(3),
	'4' | COLOR_PAIR(4),
	'5' | COLOR_PAIR(5),
	'6' | COLOR_PAIR(6),
	'7' | COLOR_PAIR(7),
	'8' | COLOR_PAIR(8),
	'P' | COLOR_PAIR(9),
	'*' | COLOR_PAIR(10),
	'*' | COLOR_PAIR(11),
	' ' | COLOR_PAIR(12)
};

struct minefield {
	int x;
	int y;
	int mines;
	char *array;
};

void new_minefield(struct minefield *ptr){
	srand(time(NULL));

	ptr->array = malloc(ptr->x * ptr->y * sizeof(char));
	for(int n = 0; n < ptr->mines; n++){
		int mine_x = rand() % ptr->x;
		int mine_y = rand() % ptr->y;
		int pos = mine_x + (mine_y * ptr->x);
		if(ptr->array[pos] == 0){ // No mine at pos
			ptr->array[pos] = 1;
		} else {
			n--; // Retry by drecrementing loop variable
		}
	}
}

char get_mine(struct minefield *ptr, int pos_x, int pos_y){
	if(pos_x < 0 || pos_x > ((ptr->x) - 1) || pos_y < 0 || pos_y > ((ptr->y) - 1)){
		return -1; // Out of bounds
	}

	return ptr->array[pos_x + (pos_y * ptr->x)];
}

int get_tile_num(struct minefield *ptr, int pos_x, int pos_y){
	int sum = 0;
	for(int dx = -1; dx < 2; dx++){
		for(int dy = -1; dy < 2; dy++){
			char mine = get_mine(ptr, pos_x + dx, pos_y + dy);
			if(mine == -1){
				// Out of bounds
			} else if(mine == 1){
				if(dx == 0 && dy == 0){
					return 10; // Mine on center
				}
				sum++;
			} else {
				// Something other than mine or no mine.
			}
		}
	}
	return sum;
}

void open(struct minefield *field, int x, int y){
	if(mvinch(y, x) == tiles[12]){
		int v = get_tile_num(field, x, y);
		mvaddch(y, x, tiles[v]);
		if(v == 0){
			open(field, x+1, y);
			open(field, x+1, y+1);
			open(field, x, y+1);
			open(field, x-1, y+1);
			open(field, x-1, y);
			open(field, x-1, y-1);
			open(field, x, y-1);
			open(field, x+1, y-1);
		}
#ifdef animated
		refresh();
		usleep(100000);
#endif
	}
}


int main(int argc, char **argv){
	int cheat=false, height=9, width=9, mines=15;
	int optchar;
	opterr = 0;

	while((optchar = getopt(argc, argv, "ch:w:m:")) != -1){
		switch(optchar){
			case 'c':
				cheat = 1;
				break;
			case 'h':
				height = atoi(optarg);
				if(height < 1){
					error("Your height must be at least 1\n");
				}
				break;
			case 'w':
				width = atoi(optarg);
				if(width < 1){
					error("Your width must be at least 1\n");
				}
				break;
			case 'm':
				mines = atoi(optarg);
				if(mines < 1){
					error("You must have at least one mine.\n");
				}
				break;
			case '?':
				if(optopt == 'h' || optopt == 'w' || optopt == 'm'){
					fprintf(stderr, "Option -%c requires an argument.\n", optopt);
				} else if(isprint(optopt)){
					fprintf(stderr, "Unknown option '-%c'.\n", optopt);
				} else {
					fprintf(stderr, "Unknown option character '\\x%x'.\n", optopt);
				}
				exit(1);
			default:
				error("Error parsing options.\n");
		}
	}

	if((height * width) <= mines){
		error("Your area must be greater than your number of mines.\n");
	}

	initscr();
	cbreak();
	noecho();
	keypad(stdscr, true);
	nodelay(stdscr, true);
	start_color();

	init_pair(1, COLOR_CYAN, COLOR_WHITE); // Number 1
	init_pair(2, COLOR_GREEN, COLOR_WHITE); // Number 2
	init_pair(3, COLOR_RED, COLOR_WHITE); // Number 3
	init_pair(4, COLOR_BLUE, COLOR_WHITE); // Number 4
	init_pair(5, COLOR_MAGENTA, COLOR_WHITE); // Number 5
	init_pair(6, 6, 7); // Number 6
	init_pair(7, 0, 7); // Number 7
	init_pair(8, 8, 7); // Number 8
	init_pair(9, 9, 15); // Flag
	init_pair(10, 0, 15); // Mine
	init_pair(11, 0, 9); // Exploded Mine
	init_extended_pair(12, COLOR_WHITE, COLOR_BLACK); // Empty
	init_pair(13, COLOR_BLACK, COLOR_WHITE); // Number 0

	struct minefield *field = malloc(sizeof(struct minefield));
	field->x = width;
	field->y = height;
	field->mines = mines;
	new_minefield(field);

	for(int x = 0; x < field->x; x++){
		for(int y = 0; y < field->y; y++){
			if(cheat){
				// Show extra map
				int v = get_tile_num(field, x, y);
				if(v == 10){
					mvwaddch(stdscr, y, x+(field->x + 5), tiles[10]);
				} else if( v <= 8 && v >= 0 ){
					mvwaddch(stdscr, y, x+(field->x + 5), tiles[v]);
				}
			}
			mvwaddch(stdscr, y, x, tiles[12]);
		}
	}

	refresh();
	wmove(stdscr, 0, 0);

	int c, v;
	int x_pos = 0;
	int y_pos = 0;
	int running = true;
	while(running){
		c = getch();
		switch(c){
			case 'q':
			case 'Q':
				running = false;
				break;
			case 'z':
				v = inch();
				if(v == tiles[12]){
					v = get_tile_num(field, x_pos, y_pos);
					if(v == 10){
						// End Game
					} else if( v >= 0 && v <= 8 ){
						open(field, x_pos, y_pos);
						wmove(stdscr, y_pos, x_pos);
					}
				}
				break;
			case 'x':
				v = inch();
				if(v == tiles[12]){
					mvwaddch(stdscr, y_pos, x_pos, tiles[9]);
				} else if(v == tiles[9]){
					mvwaddch(stdscr, y_pos, x_pos, tiles[12]);
				}
				wmove(stdscr, y_pos, x_pos);
				break;
			case KEY_UP:
				if(y_pos <= 0){
					bell();
				} else {
					wmove(stdscr, --y_pos, x_pos);
				}
				break;
			case KEY_DOWN:
				if(y_pos >= (field->y - 1)){
					bell();
				} else {
					wmove(stdscr, ++y_pos, x_pos);
				}
				break;
			case KEY_LEFT:
				if(x_pos <= 0){
					bell();
				} else {
					wmove(stdscr, y_pos, --x_pos);
				}
				break;
			case KEY_RIGHT:
				if(x_pos >= (field->x - 1)){
					bell();
				} else {
					wmove(stdscr, y_pos, ++x_pos);
				}
				break;
			case ERR:
				// no key was pressed
				break;
		}
		refresh();
		usleep(1000);
	}

	endwin();

	return 0;
}

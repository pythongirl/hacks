pub fn inspect<T>(r: &T){
    use std::mem::{transmute, discriminant, size_of};

    let discriminant: u64 = unsafe { transmute(discriminant(r)) };
    let size = size_of::<T>();
    let data: &[u8] = unsafe { std::slice::from_raw_parts(r as *const T as _, size) };

    println!("Discriminant: {}", discriminant);
    println!("Size: {}", size);
    println!("Data (hex): {:02x?}", data);
}

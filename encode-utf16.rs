use std::convert::TryFrom;
use structopt::StructOpt;

#[derive(StructOpt)]
/// Convert a Unicode scalar to UTF-16 encoded hexadecimal.
struct Opt {
    /// Unicode scalar (hexadecimal)
    input: String
}

fn main() {
    let opts = Opt::from_args();

    println!(
        "{}",
        char::try_from(
            u32::from_str_radix(&opts.input, 16)
            .expect("the input was not valid hexadecimal")
        )
            .expect("the input was not a valid codepoint")
            .encode_utf16(&mut [0u16; 2])
            .iter()
            .map(|code| format!("{:04X}", code))
            .collect::<Vec<_>>()
            .join(" ")
    );
}



#lang racket/base
(require racket/string)
(require racket/format)

(define company-name-prefixes
  #(info macro data tele super sys inter intra meta uni auto para 
         ; Less common
         epi compu graphi stat wave metro crypto trans net nova
         ; possible
         cyber digi
         
         ; Scientific
         lab gene bio
         ; SI Prefixes
         yotta zetta exa peta tera giga mega kilo milli micro nano pico femto atto zepto yocto))

(define company-name-postfixes
  #(soft com co tel tec tech dyne disk serve

         ; Less serious
         ware pro systems net max point
         storm light leaf star byte

         ; more for mechatronics things
         matics ))

(define company-name-fields
  #(software robotics research solutions technology systems group industries autonomy international electronics instruments))

; Also possible company names:
; 3-5 random letter acronyms, must be based on an acronym-sampled distribution


(define (random-element v)
  (vector-ref v (random 0 (vector-length v))))

(define (random-bool)
  (= 1 (random 2)))

(define (generate-company-name)
  (string-append
   (symbol->string (random-element company-name-prefixes))
   (symbol->string (random-element company-name-postfixes))
   " "
   (if (random-bool)
       (symbol->string (random-element company-name-fields))
       " ")
   "\n"))

(for ([i 20]) (display (generate-company-name)))

(define (term-code codes)
  (string-join (for/list ([c codes]) (~a c))
               ";"
               #:before-first "\33["
               #:after-last "m"))

(define (term-code-24bit-color r g b)
  (term-code `(38 2 ,r ,g ,b)))

; (display (format "~a      ~a" (term-code '(4)) (term-code '(0))))


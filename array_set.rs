use std::{cmp, iter, mem, ops, ptr};
use std::borrow::Borrow;

pub struct ArraySet<T> {
    length: usize,
    items: [mem::MaybeUninit<T>; 16],
}

impl<T> ArraySet<T> where T: Ord {
    pub fn new() -> ArraySet<T> {
        ArraySet {
            length: 0,
            items: unsafe { mem::MaybeUninit::uninit().assume_init() }
        }
    }

    fn set_len(&mut self, len: usize) {
        self.length = len;
    }

    pub fn len(&self) -> usize {
        self.length
    }

    pub fn capacity(&self) -> usize {
        16
    }

    pub fn as_slice(&self) -> &[T] {
        unsafe { mem::transmute::<_, &[T]>(&self.items[..self.len()]) }
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    pub fn is_disjoint(&self, other: &ArraySet<T>) -> bool {
        self.intersection(other).next().is_none()
    }

    pub fn is_subset(&self, other: &ArraySet<T>) -> bool {
        if self.len() > other.len() {
            false
        } else {
            for item in self {
                if !other.contains(item) {
                    return false
                }
            }
            true
        }
    }

    pub fn is_superset(&self, other: &ArraySet<T>) -> bool {
        other.is_subset(self)
    }

    pub fn insert(&mut self, item: T) -> Result<bool, &'static str> {
        if self.len() + 1 <= self.capacity() {
            match self.as_slice().binary_search(&item) {
                Ok(_) => Ok(false),
                Err(index) => {
                    let len = self.len();

                    // See std::Vec::insert
                    unsafe {
                        let p = self.items.as_mut_ptr().add(index);
                        ptr::copy(p, p.offset(1), len - index);
                        ptr::write(p, mem::MaybeUninit::new(item));
                    }

                    self.set_len(len + 1);

                    Ok(true)
                }
            }
        } else {
            Err("Not enough space")
        }
    }

    pub fn remove(&mut self, item: impl Borrow<T>) -> bool {
        match self.as_slice().binary_search(item.borrow()) {
            Ok(index) => {
                let len = self.len();

                // See std::Vec::remove
                unsafe {
                    let p = self.items.as_mut_ptr().add(index);
                    ptr::copy(p.offset(1), p, len - index - 1);
                }

                self.set_len(len - 1);

                true
            },
            Err(_) => false
        }
    }

    pub fn contains(&self, value: impl Borrow<T>) -> bool {
        self.as_slice().binary_search(value.borrow()).is_ok()
    }

    pub fn iter(&self) -> Iter<'_, T> {
        self.into_iter()
    }

    // pub fn range<'a, R: ops::RangeBounds<impl Borrow<T>>>(&'a self, range: R) -> <&'a [T] as IntoIterator>::IntoIter {
    //     use ops::Bound::*;

    //     let start = match range.start_bound() {
    //         Unbounded => 0,
    //         Included(b) => match self.as_slice().binary_search(b) {
    //             Ok(i) => i,
    //             Err(i) => i,
    //         },
    //         Excluded(b) => match self.as_slice().binary_search(b) {
    //             Ok(i) => i + 1,
    //             Err(i) => i,
    //         },
    //     };

    //     let end = match range.end_bound() {
    //         Unbounded => self.len(),
    //         Included(b) => match self.as_slice()[start..].binary_search(b) {
    //             Ok(i) => i + 1,
    //             Err(i) => i,
    //         }
    //         Excluded(b) => match self.as_slice()[start..].binary_search(b) {
    //             Ok(i) => i,
    //             Err(i) => i,
    //         }
    //     };

    //     self.as_slice()[start..end].iter()
    // }

    pub fn difference<'a>(&'a self, other: &'a ArraySet<T>) -> Difference<'a, T> {
        Difference {
            self_iter: self.iter(),
            other,
        }
    }

    pub fn intersection<'a>(&'a self, other: &'a ArraySet<T>) -> Intersection<'a, T> {
        Intersection {
            a_iter: self.iter(),
            b_iter: other.iter(),
        }
    }

    pub fn union<'a>(&'a self, other: &'a ArraySet<T>)-> Union<'a, T> {
        Union {
            a: self.iter().peekable(),
            b: other.iter().peekable(),
        }
    }
}

impl<'a, T: Ord> IntoIterator for &'a ArraySet<T> {
    type Item = &'a T;
    type IntoIter = <&'a [T] as IntoIterator>::IntoIter;

    fn into_iter(self) -> Self::IntoIter {
        self.as_slice().into_iter()
    }
}

type Iter<'a, T> = <&'a ArraySet<T> as IntoIterator>::IntoIter;

impl<T: Ord> iter::FromIterator<T> for ArraySet<T> {
    fn from_iter<I>(iter: I) -> ArraySet<T> where I: IntoIterator<Item = T> {
        let mut set = ArraySet::new();

        for item in iter {
            set.insert(item).expect("Too many items");
        }

        set
    }
}

pub struct Difference<'a, T: Ord> {
    self_iter: Iter<'a, T>,
    other: &'a ArraySet<T>,
}

impl<'a, T: Ord> Iterator for Difference<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<&'a T> {
        loop {
            let next = self.self_iter.next()?;
            if !self.other.contains(next) {
                return Some(next);
            }
        }
    }
}

pub struct Intersection<'a, T: Ord> {
    a_iter: Iter<'a, T>,
    b_iter: Iter<'a, T>,
}

impl<'a, T: Ord> Iterator for Intersection<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<&'a T> {
        let mut a_next = self.a_iter.next()?;
        let mut b_next = self.b_iter.next()?;

        loop {
            match a_next.cmp(b_next) {
                cmp::Ordering::Less => a_next = self.a_iter.next()?,
                cmp::Ordering::Equal => return Some(a_next),
                cmp::Ordering::Greater => b_next = self.b_iter.next()?,
            }
        }
    }
}

pub struct Union<'a, T: Ord> {
    a: iter::Peekable<Iter<'a, T>>,
    b: iter::Peekable<Iter<'a, T>>,
}

impl<'a, T: Ord> Iterator for Union<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<&'a T> {
        match (self.a.peek(), self.b.peek()) {
            (Some(a), Some(b)) => match a.cmp(b) {
                cmp::Ordering::Less => self.a.next(),
                cmp::Ordering::Equal => {
                    self.b.next();
                    self.a.next()
                },
                cmp::Ordering::Greater => self.b.next(),
            },
            (Some(_), None) => self.a.next(),
            (None, Some(_)) => self.b.next(),
            (None, None) => None,
        }
    }
}



fn main() -> Result<(), &'static str> {
    let set1 = vec![1, 2, 3].into_iter().collect::<ArraySet<u8>>();
    let set2 = vec![1, 3, 4].into_iter().collect::<ArraySet<u8>>();

    println!("{:?}", set2.union(&set1).collect::<Vec<_>>());

    println!("{:?}", set1.contains(2));

    let set3 = vec![1, 3, 5].into_iter().collect::<ArraySet<u8>>();
    let set4 = vec![0, 2, 4].into_iter().collect::<ArraySet<u8>>();

    println!("{:?}", set3.is_disjoint(&set4));


    Ok(())
}
